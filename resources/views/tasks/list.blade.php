@extends('layouts.app')

@section('title', 'List')

@section('content')
    <div class="m-4">
        <span class="font-size-1 font-bold"> Ini halaman Task List </span>
        @isset( $status )
        <span> 
            @if($status == 'delete') 
                task deleted! 
            @elseif($status == 'checked') 
                task updated! 
            @endif 
        </span>
        @endisset
    </div>
    <div class="mx-4 mt-2">
        <table id="taskList" class="table table-striped">
            <thead>
                <tr>
                    <td> No </td>
                    <td> Task Name </td>
                    <td> Description </td>
                    <td> Status </td>
                    <td> Image </td>
                    <td> Options </td>
                </tr>
            </thead>
            <tbody>
                <!-- display task list -->
                @foreach($myTasks as $key => $task)
                <tr class="mutable-row" data-item-id="{{ $task->id }}">
                    <td> {{ $key + 1 }} </td>
                    <!-- task name -->
                    <td> {{ $task->name }} </td>
                    <!-- description -->
                    <td> {{ $task->description }} </td>
                    <!-- checkbox -->
                    <td class="mutable-col"> 
                        <label for="status"> 
                            @if($task->status == 'belum')
                                <input type="checkbox" id="status" name="status" value="{{ $task->status }}" onchange="updateStatusEvent(event)" data-route="{{ route('list.update', ['id' => $task->id]) }}">
                            @elseif($task->status == 'selesai')
                                <input type="checkbox" id="status" name="status" value="{{ $task->status }}" onchange="updateStatusEvent(event)" data-route="{{ route('list.update', ['id' => $task->id]) }}" checked>
                            @endif
                            {{ $task->status }} 
                        </label>
                    </td>
                    <!-- image -->
                    <td> {{ $task->image }} </td>
                    <!-- options -->
                    <td class="w-25">
                        <!-- delete button -->
                        <form class="d-inline" action="{{ route('list.delete', ['id' => $task->id]) }}" method="POST" onsubmit="deleteTaskEvent(event)">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger"> Delete </button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection