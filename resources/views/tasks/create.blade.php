@extends('layouts.app')

@section('title', 'List')

@section('content')
    <div class="m-4">
        <span class="font-size-1 font-bold"> Ini halaman Create Task </span>
    </div>
    <form id="createTaskForm" class="mx-4 mt-2 w-50 needs-validation" action="{{ route('create.post') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="mt-2">
            <label for="name" class="form-label"> Task Name </label>
            <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" placeholder="task name" required>
            @error('name')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="mt-2">
            <label for="description" class="form-label"> Description (optional) </label>
            <input type="text" class="form-control @error('description') is-invalid @enderror" id="description" name="description" placeholder="task description">
            @error('description')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="mt-2">
            <label for="image" class="form-label"> Image (optional) </label>
            <input type="file" class="form-control @error('image') is-invalid @enderror" id="image" name="image" placeholder="task image">
            @error('image')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="mt-4 d-flex justify-content-between">
            <button class="btn btn-primary" type="submit">Create Task</button>
        </div>
        @isset( $status )
        <span> {{ $status == 'insert' ? 'task created!' : null }} </span>
        @endisset
    </form>
@endsection