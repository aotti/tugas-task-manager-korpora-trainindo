@extends('layouts.app')

@section('title', 'Home')

@section('content')
    <div class="mx-4 mt-4">
        <span class="font-size-1 font-bold"> Selamat datang, {{ Auth::user()->name }}! </span>
    </div>
@endsection