@extends('layouts.app')

@section('title', 'Register')

@section('content')
    <div class="mx-4 mt-4">
        <span class="font-size-1 font-bold"> Ini halaman Register </span>
    </div>
    <form id="registerForm" class="mx-4 mt-2 w-50 needs-validation" action="{{ route('register') }}" method="POST">
        @csrf
        <div class="mt-2">
            <label for="name" class="form-label"> Name </label>
            <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" placeholder="full name" required>
            @error('name')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="mt-2">
            <label for="email" class="form-label"> Email </label>
            <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email" placeholder="name@email.com" required>
            @error('email')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="mt-2">
            <label for="password" class="form-label"> Password </label>
            <input type="password" class="form-control @error('password') is-invalid @enderror" id="password" name="password" placeholder="password" required>
            @error('password')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="mt-2">
            <label for="password_confirmation" class="form-label"> Password Confirmation </label>
            <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="confirm password" required>
        </div>
        <div class="mt-4 d-flex justify-content-between">
            <button class="btn btn-primary" type="submit">Register</button>
            <a href="{{ url('/') }}"> Back to first page </a>
        </div>
    </form>
@endsection