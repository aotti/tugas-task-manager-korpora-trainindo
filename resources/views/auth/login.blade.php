@extends('layouts.app')

@section('title', 'Login')

@section('content')
    <div class="m-left-4 m-top-4">
        <span class="font-size-1 font-bold"> Ini halaman Login </span>
    </div>
    <form id="loginForm" class="mx-4 mt-2 w-50 needs-validation" action="{{ route('login') }}" method="POST">
        @csrf
        <div class="mt-2">
            <label for="email" class="form-label"> Email </label>
            <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email" placeholder="name@email.com" required>
            @error('email')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="mt-2">
            <label for="password" class="form-label"> Password </label>
            <input type="password" class="form-control @error('password') is-invalid @enderror" id="password" name="password" placeholder="password" required>
            @error('password')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="mt-4 d-flex justify-content-between">
            <button class="btn btn-primary" type="submit">Login</button>
            <a href="{{ url('/') }}"> Back to first page </a>
        </div>
    </form>
@endsection