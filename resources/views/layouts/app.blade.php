<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script><script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <title> Task Manager | @yield('title') </title>
</head>
<body>
    <!-- HEADER -->
    @if( $view == 'login' || $view == 'register' || $view == 'index' )
    <header class="bg-blue">
        <div class="p-2"> </div>
    </header>
    @else
    <header class="bg-blue">
        <div class="d-flex justify-content-between mx-auto mx-lg-0 p-2 w-50">
            <nav id="navMenu" class="d-lg-flex justify-content-around align-items-center bor-s bor-rad mr-4 w-100">
                <a href="{{ route('home') }}" class="d-block text-decoration-none text-center text-light font-bold border rounded-left pt-2 pb-2 pt-sm-2 pb-sm-2 w-100"> Home </a>
                <a href="{{ route('create.get') }}" class="d-block text-decoration-none text-center text-light font-bold border t-2 pb-2 pt-sm-2 pb-sm-2 w-100"> Create Task </a>
                <a href="{{ route('list.get', ['user_id' => Auth::user()->id]) }}" class="d-block text-decoration-none text-center text-light font-bold border t-2 pb-2 pt-sm-2 pb-sm-2 w-100"> Task List </a>
                <a href="{{ url('logout') }}" class="d-block text-decoration-none text-center text-light font-bold border rounded-right pt-2 pb-2 pt-sm-2 pb-sm-2 w-100" onclick="logoutEvent(event)"> Logout </a>
                <form id="logoutForm" action="{{ route('logout') }}" method="POST">@csrf</form>
            </nav>
        </div>
    </header>
    @endif
    <!-- MAIN -->
    @yield('content')
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>