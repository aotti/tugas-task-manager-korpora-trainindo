@extends('layouts.app')

@section('title', 'Index')

@section('content')
    <div class="mx-4 mt-4">
        <span class="font-size-1 font-bold"> Silahkan <a href="{{ route('login') }}">Login</a> untuk menggunakan fitur Task Manager </span>
    </div>
    <div class="mx-4 mt-4">
        <span class="font-size-1 font-bold"> Silahkan <a href="{{ route('register') }}">Register</a> jika belum punya akun </span>
    </div>
@endsection