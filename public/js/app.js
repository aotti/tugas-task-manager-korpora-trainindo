// logout handler 
function logoutEvent(ev) {
    // prevent from click <a> tag
    ev.preventDefault()
    // submit form instead
    $('#logoutForm').submit()
}

function inputAttributes(input, attr) {
    // get object keys
    const key = Object.keys(attr)
    // get object values
    const value = Object.values(attr)
    // set attributes for the input
    for(let i in key) {
        switch(key[i]) {
            case 'id':
                input.id = value[i]
                break
            case 'type':
                input.type = value[i]
                break
            case 'name':
                input.name = value[i]
                break
            case 'value':
                input.value = value[i]
                break
            case 'setAttribute_1':
                input.setAttribute(value[i][0], value[i][1])
                break
            case 'setAttribute_2':
                input.setAttribute(value[i][0], value[i][1])
                break
        }
    }
    return input
}

// delete table row on task list
function deleteTableCells(res) {
    const getRowForDelete = Object.values($('tr')).map(v => { if(v.dataset?.itemId == res.id) return v }).filter(i=>i)[0]
    const taskListTable = $('#taskList')
    taskListTable[0].deleteRow(getRowForDelete.rowIndex)
}

// update table cells on task list
function updateTableCells(res) {
    const cE = (el) => { return document.createElement(el) }
    // get tbody
    const mutableRows = $('.mutable-row')
    const colData = []
    for(let i in res) {
        // create checkbox
        const checkboxInput = inputAttributes(cE('input'), 
            { 
                type: 'checkbox', name: 'status', id: 'status', 
                // set event attribute to run updateStatusEvent
                setAttribute_1: ['onchange', 'updateStatusEvent(event)'], 
                // set route for ajax url
                setAttribute_2: ['data-route', `${window.location.origin}/list/item/${res[i].id}`] 
            }
        )
        // uncheck if status == belum, check if status == selesai
        res[i].status == 'belum' ? checkboxInput.checked = false : checkboxInput.checked = true
        // create label
        const label = cE('label')
        label.for = 'status'
        label.innerText = ` ${res[i].status}`
        // append checkbox to label
        label.insertBefore(checkboxInput, label.firstChild)
        // put data for column to array
        colData[i] = [label]
    }
    // row
    for(let j in mutableRows) {
        if(mutableRows[j].nodeName == 'TR') {
            // refill columns with colData values
            const mutableColumns = Object.values(mutableRows[j].cells).map(v => { if(v.className == 'mutable-col') return v }).filter(i=>i)
            for(let k in mutableColumns) {
                // remove element inside bfore append
                mutableColumns[k].innerText = ''
                // if data is html element = append data, else innerText data
                typeof colData[k] == 'object' 
                ? mutableColumns[k].append(colData[j][k]) 
                : mutableColumns[k].innerText = colData[j][k]
            }
        }
    }
}

// ajax csrf token setup
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr('content')
    }
})

// delete task ajax handler
function deleteTaskEvent(ev) {
    // prevent submit form normally
    ev.preventDefault()
    // start ajax
    $.ajax({
        url: ev.target.action,
        type: 'DELETE',
        success: function(res) {
            deleteTableCells(res)
        }
    })
}

// update status ajax handler
function updateStatusEvent(ev) {
    ev.preventDefault()
    // for update checkbox status
    const checkboxStatus = ev.target.checked === true ? 'selesai' : 'belum'
    // start ajax
    $.ajax({
        url: ev.target.dataset.route,
        type: 'PATCH',
        data: { status: checkboxStatus },
        success: function(res) {
            updateTableCells(res)
        }
    })
}
