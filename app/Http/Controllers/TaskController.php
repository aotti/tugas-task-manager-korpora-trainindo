<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Task;

class TaskController extends Controller
{
    public function index() {
        return view('tasks.create', ['view' => 'create']);
    }

    // input data
    public function create(Request $request) {
        // validate create task inputs
        $request->validate([
            'name' => 'required|string|max:255',
            'description' => 'nullable|string|max:255',
            'image' => 'nullable|mimes:jpeg,jpg,png,JPEG,JPG,PNG'
        ]);
        // get image file path
        if(isset($request->image))
            $path = $request->file('image')->store('images');
        // insert create task values to database
        Task::create([
            'user_id' => Auth::user()->id,
            'name' => $request->name,
            'description' => $request->description,
            'status' => 'belum',
            'image' => $path ?? null
        ]);
        // back to create task page
        return view('tasks.create', ['view' => 'create', 'status' => 'insert']);
    }

    // get data by user_id
    public function show($user_id) {
        // get all data 
        $myTasks = Task::where('user_id', $user_id)->orderBy('id', 'ASC')->get();
        // send retrieved data to view
        return view('tasks.list', ['view' => 'list', 'myTasks' => $myTasks]);
    }

    // delete data
    public function delete($id) {
        // find data by id
        $myTask = Task::find($id);
        // delete the data
        $myTask->delete();
        // return all data to ajax
        return response()->json($myTask);
    }

    // update status
    public function update(Request $request, $id) {
        // get data for update
        $myTask = Task::where('id', $id)->first();
        $myTask->status = $request->status;
        $myTask->save();
        // get all data after update
        $myTasks = Task::where('user_id', Auth::user()->id)->orderBy('id', 'ASC')->get();
        // return all data to ajax
        return response()->json($myTasks);
    }
}
