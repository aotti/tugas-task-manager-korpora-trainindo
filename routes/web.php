<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TaskController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// public routes, anyone can access
Route::get('/', function () {
    return view('index', ['view' => 'index']);
});

// auth routes, need authentication to access this routes
Route::group(['middleware' => ['auth']], function() {
    Route::get('/home', function() {
        return view('tasks.home', ['view' => 'home']);
    })->name('home');
    
    Route::get('/create', [TaskController::class, 'index'])->name('create.get');
    Route::post('/create', [TaskController::class, 'create'])->name('create.post');
    
    Route::get('/list/user/{user_id}', [TaskController::class, 'show'])->name('list.get');
    Route::delete('/list/item/{id}', [TaskController::class, 'delete'])->name('list.delete');
    Route::patch('/list/item/{id}', [TaskController::class, 'update'])->name('list.update');
});
